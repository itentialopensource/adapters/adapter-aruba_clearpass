
## 0.5.6 [11-25-2024]

* fix auth docs

See merge request itentialopensource/adapters/adapter-aruba_clearpass!16

---

## 0.5.5 [10-15-2024]

* Changes made at 2024.10.14_20:32PM

See merge request itentialopensource/adapters/adapter-aruba_clearpass!15

---

## 0.5.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aruba_clearpass!13

---

## 0.5.3 [08-14-2024]

* Changes made at 2024.08.14_18:46PM

See merge request itentialopensource/adapters/adapter-aruba_clearpass!12

---

## 0.5.2 [08-07-2024]

* Changes made at 2024.08.06_19:59PM

See merge request itentialopensource/adapters/adapter-aruba_clearpass!11

---

## 0.5.1 [08-06-2024]

* Changes made at 2024.08.06_12:52PM

See merge request itentialopensource/adapters/adapter-aruba_clearpass!10

---

## 0.5.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!9

---

## 0.4.4 [03-27-2024]

* Changes made at 2024.03.27_13:45PM

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!8

---

## 0.4.3 [03-13-2024]

* Changes made at 2024.03.13_13:46PM

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!7

---

## 0.4.2 [03-11-2024]

* Changes made at 2024.03.11_16:16PM

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!6

---

## 0.4.1 [02-27-2024]

* Changes made at 2024.02.27_11:50AM

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!5

---

## 0.4.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!4

---

## 0.3.1 [09-19-2022]

* Fix data type for number in pronghorn.json

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!3

---

## 0.3.0 [05-19-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!2

---

## 0.2.0 [01-21-2022]

- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/security/adapter-aruba_clearpass!1

---

## 0.1.1 [06-22-2021]

- Initial Commit

See commit 38e410c

---
